#!/usr/bin/env python
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.urn as URN
import geni.rspec.emulab.pnext as PN
import geni.rspec.igext as IG


tourDescription = """

# General LTE

This profile includes the following resources in a lab setting:

  * SDR UE (d740 compute + X310 USRP)
  * SDR eNB (d740 compute + x310 USRP)
  * RawPC for EPC

The d740s use a low-latency kernel and are connected to a datastore containing
srsLTE, OAI, and UHD sources.

"""

tourInstructions = """

Build the desired stack from the sources located at `/opt/sources`.

"""


class GLOBALS(object):
    LTE_SRC_DS = "urn:publicid:IDN+emulab.net:powderteam+imdataset+lte-and-uhd-src"
    UBUNTU_18LL_IMG = "urn:publicid:IDN+emulab.net+image+PowderProfiles:ubuntu1804lowlatency"
    UBUNTU_18_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
    COMPUTE_TYPE = "d740"


def connect_datastore(node):
    bs = node.Blockstore("ds-%s" % node.name, "/opt/sources")
    bs.dataset = GLOBALS.LTE_SRC_DS
    bs.rwclone = True

pc = portal.Context()
pc.defineParameter("ENODEB_COMP", "Compute node for eNB",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Input the name of a specific compute node to allocate for the eNB. Leave blank to use default or to let the mapping algorithm choose.")
pc.defineParameter("ENODEB_SDR", "SDR for eNB",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Input the name of a specific SDR to allocate for the eNB. Leave blank to use the default or to let the mapping algorithm choose.")
pc.defineParameter("UE_COMP", "Compute node for UE",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Input the name of a specific compute node to allocate for the UE. Leave blank to use default or to let the mapping algorithm choose.")
pc.defineParameter("UE_SDR", "SDR for UE",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Input the name of a specific SDR to allocate for the UE. Leave blank to use the default or to let the mapping algorithm choose.")

params = pc.bindParameters()
pc.verifyParameters()

request = pc.makeRequestRSpec()
epclink = request.Link("s1-lan")

enb1 = request.RawPC("enb1")
connect_datastore(enb1)
if params.ENODEB_COMP:
    enb1.component_id = params.ENODEB_COMP
else:
    enb1.hardware_type = GLOBALS.COMPUTE_TYPE

enb1.disk_image = GLOBALS.UBUNTU_18LL_IMG
enb1_usrp_if = enb1.addInterface("usrp-if")
enb1_usrp_if.addAddress(rspec.IPv4Address("192.168.30.1", "255.255.255.0"))
epclink.addNode(enb1)

# Add X310 USRP for eNB
usrp_enb = request.RawPC("usrp-enb")
if params.ENODEB_SDR:
    usrp_enb.component_id = params.ENODEB_SDR
else:
    usrp_enb.component_id = 'pnbase1'

usrp_enb.hardware_type = "sdr"
usrp_enb.disk_image = URN.Image(PN.PNDEFS.PNET_AM, "emulab-ops:GENERICDEV-NOVLANS")
usrp_enb_if = usrp_enb.addInterface("usrp-enb-if")
usrp_enb_if.addAddress(rspec.IPv4Address("192.168.30.2", "255.255.255.0"))

usrp_link_enb = request.Link("usrp-link-enb" )
usrp_link_enb.addInterface(enb1_usrp_if)
usrp_link_enb.addInterface(usrp_enb_if)

# Add compute for UE
rue1 = request.RawPC("rue1")
connect_datastore(rue1)
if params.UE_COMP:
    rue1.component_id = params.UE_COMP
else:
    rue1.hardware_type = GLOBALS.COMPUTE_TYPE

rue1.disk_image = GLOBALS.UBUNTU_18LL_IMG
rue1_usrp_if = rue1.addInterface("usrp-if")
rue1_usrp_if.addAddress(rspec.IPv4Address("192.168.30.1", "255.255.255.0"))

# Add X310 USRP for UE
usrp_ue = request.RawPC("usrp-ue")
if params.UE_SDR:
    usrp_ue.component_id = params.UE_SDR
else:
    usrp_ue.component_id = 'pnbase2'

usrp_ue.hardware_type = "sdr"
usrp_ue.disk_image = URN.Image(PN.PNDEFS.PNET_AM, "emulab-ops:GENERICDEV-NOVLANS")
usrp_ue_if = usrp_ue.addInterface("usrp-nuc")
usrp_ue_if.addAddress(rspec.IPv4Address("192.168.30.2", "255.255.255.0"))

usrp_link_ue = request.Link("usrp-link-ue")
usrp_link_ue.addInterface(rue1_usrp_if)
usrp_link_ue.addInterface(usrp_ue_if)

epc = request.RawPC("epc")
connect_datastore(epc)
epc.disk_image = GLOBALS.UBUNTU_18_IMG

epclink.addNode(epc)
epclink.link_multiplexing = True
epclink.vlan_tagging = True
epclink.best_effort = True

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

pc.printRequestRSpec(request)
